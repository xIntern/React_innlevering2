import React from 'react';
import { StatusBar } from 'react-native';
import { Scene, Router } from 'react-native-router-flux';
import Home from './Components/Home';
import ItemsList from "./Components/ItemsList";

export default class App extends React.Component {

  constructor() {
    super();
  }

  render() {
    return (
      <Router sceneStyle={styles.statusBar}>
        <Scene key="root">
          <Scene key="itemsList" component={ItemsList} title="Varer" back />
          <Scene key="home" component={Home} title="Handlelister" initial />
        </Scene>
      </Router>
    );
  }
}

const styles = {
  statusBar: {
    marginTop: StatusBar.currentHeight || 0
  }
}
