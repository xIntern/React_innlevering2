import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { FormInput, Button } from 'react-native-elements';
import LogIn from './LogIn';
import Register from './Register';

export default class NotLoggedIn extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      register: false
    };

    this.toggleRegister = this.toggleRegister.bind(this);
  }

  toggleRegister() {
    this.setState({register: !this.state.register})
  }

  render() {
    return (
      <View>
        {!this.state.register ? (
          <View>
            <LogIn loginFn={this.props.loginFn} />
            <Button
              large
              raised
              backgroundColor="#9E9E9E"
              containerViewStyle={{marginTop: 20}}
              title="Ny bruker"
              onPress={() => this.toggleRegister()}
            />
          </View>
        ) : (
          <Register toggleRegister={this.toggleRegister} />
        )}
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 25
  }
});
