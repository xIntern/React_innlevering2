import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { FormInput, Button } from 'react-native-elements';

export default class LogIn extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }

  componentDidMount() {

  }

  render() {
    return (
      <View style={style.container}>
        <FormInput
          placeholder="E-post"
          keyboardType="email-address"
          returnKeyType="next"
          onChangeText={value => this.setState({email: value})}
        />
        <FormInput
          placeholder="Passord"
          secureTextEntry={true}
          returnKeyType="done"
          onChangeText={value => this.setState({password: value})}
        />
        <Button
          large
          containerViewStyle={{width: '100%', paddingLeft: 15, paddingRight: 15}}
          backgroundColor="#607D8B"
          title="Logg inn"
          onPress={() => this.props.loginFn(this.state)}
        />
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 25
  }
});
