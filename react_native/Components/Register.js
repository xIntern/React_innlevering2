import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { FormInput, Button } from 'react-native-elements';

export default class Register extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {

  }

  render() {
    return (
      <View style={style.container}>
        <FormInput
          placeholder="Navn"
          keyboardType="default"
          returnKeyType="next"
        />
        <FormInput
          placeholder="E-post"
          keyboardType="email-address"
          returnKeyType="next"
        />
        <FormInput
          placeholder="Passord"
          secureTextEntry={true}
          returnKeyType="next"
        />
        <FormInput
          placeholder="Gjenta passord"
          secureTextEntry={true}
          returnKeyType="done"
        />
        <Button
          large
          containerViewStyle={style.btnFullWidth}
          backgroundColor="#607D8B"
          title="Opprett bruker"
        />
        <Button
          large
          containerViewStyle={[style.btnFullWidth, {marginTop: 20}]}
          backgroundColor="#9E9E9E"
          title="Avbryt"
          onPress={() => this.props.toggleRegister()}
        />
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 25
  },
  btnFullWidth: {
    width: '100%',
    paddingLeft: 15,
    paddingRight: 15
  }
});
