import React from 'react';
import { AsyncStorage, StyleSheet, View, Image, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Avatar, Card, List, ListItem, Text, Icon } from 'react-native-elements';
import Swipeable from 'react-native-swipeable';
import ActionButton from 'react-native-action-button';

const styles = StyleSheet.create({
  image: {
    display: 'flex',
    width: 60,
    height: 60
  },
  button: {
    maxWidth: 80,
    alignItems: 'center',
    backgroundColor: 'red'
  },
  buttonText: {
    padding: 15,
    color: 'white'
  }
});

const apiBase = 'http://192.168.1.7:3001';

export default class Items extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: this.props.token,
      list: null
    }
    this.getShoppingList(this.props.list._id);
  }

  getShoppingList(id) {
    return fetch(`${apiBase}/shopping-list/${id}`, {
      headers: {
        'Authorization': this.state.token
      }
    }).then(response => {
      if (!response.ok) {
        console.error('Response: ', response);
        return;
      }
      return response;
    }).then(json => json.json()).then(list => {
      if (!list) {
        console.log('Empty item list');
        return;
      }
      this.setState({ list: list });
      console.log(list);
    }).catch(err => {
      console.error(err);
    });
  }

  render() {
    return (
      <View style={{ flex: 1, marginTop: -21 }}>
        {this.state.list !== null &&
          <List>
            {this.state.list.items.map(item => <Swipeable
              key={item._id}
              rightButtons={[
                (<TouchableHighlight onPress={() => this.deleteItemFromList(item._id)}>
                  <View style={styles.button}>
                    <Icon iconStyle={styles.buttonText} name="delete" />
                  </View>
                </TouchableHighlight>)
                ]}
            >
                <ListItem
                  hideChevron
                  title={item.item.name}
                  avatar={{uri: item.item.image}}
                  avatarStyle={{resizeMode: "contain", backgroundColor: '#fff'}}
            /></Swipeable>)}
          </List>
        }
        <ActionButton
          buttonColor="#00c853"
          onPress={() => {
            console.log('Open add item modal');
          }}
        />
      </View>
    );
  }
}

