import React from 'react';
import { AsyncStorage, StyleSheet, Text, View, StatusBar } from 'react-native';
import { Header, Icon } from 'react-native-elements';
import ShoppingLists from './ShoppingLists';
import NotLoggedIn from './NotLoggedIn';

const apiBase = 'http://192.168.1.7:3001';

export default class App extends React.Component {

  constructor() {
    super();
    this.state = {
      isLoggedIn: false,
      token: null
    };

    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }

  login(formData) {
    fetch(`${apiBase}/login`, {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData)
    }).then(response => {
      if (!response.ok) {
        console.log(response);
        return Promise.reject('Something went wrong!');
      }
      return response;
    }).then(json => json.json()).then(response => {
      AsyncStorage.setItem('token', response.token);
      this.setState({isLoggedIn: true});
    }).catch(err => {
    });
  }

  logout() {
    AsyncStorage.removeItem('token');
    this.setState({isLoggedIn: false, token: null});
  }

  async componentDidMount() {
    const token = await AsyncStorage.getItem('token');
    if (token) {
      this.setState({isLoggedIn: true, token: token});
    }  else {
      this.setState({isLoggedIn: false});
    }
    // this.logout();
  }

  render() {
    console.log(StatusBar.setHidden(false));
    return (
      <View style={styles.container}>
        {this.state.isLoggedIn ? (
          <ShoppingLists token={this.state.token} />
        ) : (
          <NotLoggedIn loginFn={this.login} registerFn={this.register} />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  }
});
