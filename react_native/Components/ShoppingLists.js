import React from 'react';
import { AsyncStorage, StyleSheet, View, Image, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Avatar, Card, List, ListItem, Text, Icon } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import Swipeable from 'react-native-swipeable';
import ActionButton from 'react-native-action-button';

const apiBase = 'http://192.168.1.7:3001';

export default class ShoppingLists extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: this.props.token,
      lists: [],
      openList: null,
      isLoggedIn: false
    };

    this.deleteItemFromList = this.deleteItemFromList.bind(this);
  }

  getShoppingLists() {
    fetch(`${apiBase}/shopping-lists`, {
      headers: {
        'Authorization': this.state.token
      }
    }).then(response => {
      if (!response.ok) {
        return response.json().then(json => Promise.reject(json));
      }
      return response;
    }).then(json => json.json()).then(lists => {
      if (!lists) {
        return Promise.reject({ error: { message: 'Oops! Something went wrong. Could not find any lists' } });
      }
      this.setState({ lists: lists });
    }).catch(err => {
      this.refs.toast.show(err.error.message, 2000);
    });
  }

  getShoppingList(id) {
    return fetch(`${apiBase}/shopping-list/${id}`, {
      headers: {
        'Authorization': this.state.token
      }
    }).then(response => {
      if (!response.ok) {
        return response.json().then(json => Promise.reject(json));
      }
      return response;
    }).then(json => json.json()).then(list => {
      if (!list) {
        return Promise.reject({ error: { message: 'Oops! Something went wrong. List is empty' } });
      }
      this.setState({ openList: list });
    }).catch(err => {
      this.refs.toast.show(err.error.message, 2000);
    });
  }

  deleteItemFromList(itemId) {
    fetch(`${apiBase}/shopping-lists/${this.state.openList._id}/item/${itemId}`, {
      method: 'DELETE',
      headers: { 'Authorization': this.state.token }
    }).then(response => {
      if (response.status !== 204) {
        return response.json().then(json => Promise.reject(json));
      }
      this.setState(prevState => {
        const itemIndex = prevState.openList.items.findIndex(item => item._id === itemId);
        prevState.openList.items.splice(itemIndex, 1);
        return {openList: prevState.openList};
      });
    }).catch(err => {
      this.refs.toast.show(err.error.message, 2000);
    });
  }

  closeList() {
    this.setState({ openList: null })
  }

  componentDidMount() {
    this.getShoppingLists();
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <List style={{marginBottom: 20}}>
          {this.state.lists.map(list => <ListItem
            key={list._id}
            title={list.name}
            badge={{
              value: list.items.length,
              textStyle: {
                color: '#333'
              },
              containerStyle: {
                backgroundColor: '#fff'
              }
            }}
            onPress={() => {
              Actions.itemsList({ title: list.name, token: this.state.token, list: list, deleteItemFn: this.deleteItemFromList });
            }}
          />)}
        </List>
        <ActionButton
          buttonColor="#00c853"
          onPress={() => { console.log("hi")}}
        />
        <Toast
            ref="toast"
            style={{backgroundColor:'red'}}
            position='top'
            positionValue={0}
            fadeInDuration={250}
            fadeOutDuration={500}
            opacity={0.8}
            textStyle={{color:'white'}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    display: 'flex',
    width: 60,
    height: 60
  },
  button: {
    maxWidth: 80,
    alignItems: 'center',
    backgroundColor: 'red'
  },
  buttonText: {
    padding: 15,
    color: 'white'
  }
});
