FROM node:8-alpine

WORKDIR /usr/src/app

COPY package*.json ./
COPY server/ ./server/

RUN npm i

EXPOSE 80

CMD ["node", "server/main.js"]
