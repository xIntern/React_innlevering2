import React, { Component } from 'react';
import Nav from './Components/Nav/Nav';
import LoggedIn from './Components/LoggedIn';
import NotLoggedIn from './Components/NotLoggedIn';
import KolonialFooter from './Components/Footer/KolonialFooter';
import Modal from './Components/Modal/Modal';
import Backdrop from './Components/Modal/Backdrop';
import 'materialize-css/dist/css/materialize.min.css';
import 'materialize-css/dist/js/materialize.min.js';

const apiBase = 'http://localhost:3001';

class App extends Component {
    constructor() {
        super();
        this.state = {
            user: {},
            loggedIn: (typeof(localStorage.token) !== 'undefined'),
            modalForm: 'login',
            isModalOpen: false,
            authError: null,
        };
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.register = this.register.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.closeAuthError = this.closeAuthError.bind(this);
        this.handleLoginRegForm = this.handleLoginRegForm.bind(this);
    }

    login(userObject) {
        fetch(`${apiBase}/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userObject)
        }).then(response => {
            if (!response.ok) {
                return response.json().then(json => Promise.reject(json));
            }
            return response;
        }).then(response => response.json()).then(json => {
            localStorage.setItem('token', json.token);
            localStorage.setItem('user', JSON.stringify(json.user));
            this.setState({ user: json.user, loggedIn: true, isModalOpen: false });
        }).catch(err => {
            this.setState({authError: err.error});
        });
    }

    register(userObject) {
        fetch(`${apiBase}/users`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userObject)
        }).then(response => {
            if (!response.ok) {
                return response.json().then(json => Promise.reject(json));
            }
            return response;
        }).then(response => response.json()).then(json => {
            localStorage.setItem('token', json.token);
            localStorage.setItem('user', JSON.stringify(json.user));
            this.setState({ user: json.user, loggedIn: true, isModalOpen: false });
        }).catch(err => {
            this.setState({authError: err.error});
        });
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        this.setState({ user: {}, loggedIn: false });
    }

    handleLoginRegForm(formData) {
        if (this.state.modalForm === 'registerForm') {
            if (formData.password !== formData['repeat-password']) {
                this.showAlertMessage('Form error', 'Passwords doesnt match..!');
                return;
            }
            this.register(formData);
        } else if (this.state.modalForm === 'loginForm') {
            this.login(formData);
        }
    }

    closeAuthError() {
        this.setState({authError: null});
    }

    toggleModal(form = false) {
        if (!form) {
            this.setState({ isModalOpen: false });
        }
        this.setState({ modalForm: form, isModalOpen: !this.state.isModalOpen });
    }

    componentDidMount() {
        if (typeof(localStorage.user) !== 'undefined') {
            const user = JSON.parse(localStorage.user);
            this.setState({ user: { name: user.name, email: user.email } });
        }
    }

    render() {
        return (
            <div className="App">
                <Nav toggleModal={this.toggleModal} logoutFn={this.logout} user={this.state.user} loggedIn={this.state.loggedIn} />
                <KolonialFooter />
                {this.state.loggedIn ? (
                    <LoggedIn />
                ) : (
                    <NotLoggedIn registerFn={this.register} loginFn={this.login} error={this.state.authError} closeError={this.closeAuthError} />
                )}
                {this.state.isModalOpen && <Backdrop closeModal={this.toggleModal} />}
                {this.state.isModalOpen && <Modal handleSubmit={this.handleLoginRegForm} component={this.state.modalForm} closeModal={this.toggleModal} />}
            </div>
        );
    }
}

export default App;
