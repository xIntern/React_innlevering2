import React, { Component } from 'react';
import Modal from './Modal/Modal';
import Backdrop from './Modal/Backdrop';
import ShoppingLists from './ShoppingList/ShoppingLists';
import ShoppingListsPublic from './ShoppingList/ShoppingListsPublic';
import ShoppingListContent from './ShoppingList/ShoppingListContent';

const apiBase = 'http://localhost:3001';

class LoggedIn extends Component {

	constructor(props) {
		super(props);
		this.state = {
			ws: null,
			lists: [],
			openList: {},
			toggleOpenShoppingList: null,
			showModal: false,
			modalComponent: ''
		};

		this.addItemToList = this.addItemToList.bind(this);
		this.addShoppingList = this.addShoppingList.bind(this);
		this.deleteItemFromList = this.deleteItemFromList.bind(this);
		this.deleteShoppingList = this.deleteShoppingList.bind(this);
		this.toggleOpenShoppingList = this.toggleOpenShoppingList.bind(this);
		this.publishShoppingListToggle = this.publishShoppingListToggle.bind(this);
		this.toggleOpenShoppingListModal = this.toggleOpenShoppingListModal.bind(this);
	}

	getShoppingLists() {
		fetch(`${apiBase}/shopping-lists`, {
			headers: {
				'Authorization': localStorage.token
			}
		}).then(response => {
			if (!response.ok) {
				return response.json().then(json => Promise.reject(json));
			}
			return response;
		}).then(json => json.json()).then(lists => {
			if (!lists) {
				return Promise.reject({ error: { message: 'Oops! Something went wrong. Lists is empty' } });
			}
			this.setState({ lists: lists });
		}).catch(err => {
			window.M.toast({classes: 'red', html: err.error.message});
		});
	}

	addItemToList(itemId, itemAmount = 1) {
		if (JSON.parse(localStorage.user).id !== this.state.toggleOpenShoppingList.addedBy.user) {
			window.M.toast({classes: 'red', html: 'You are not permitted to edit this list'});
		}
		fetch(`${apiBase}/shopping-lists/${this.state.toggleOpenShoppingList._id}`, {
			method: 'POST',
			headers: {
				'Authorization': localStorage.token,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				item: itemId,
				amount: itemAmount
			})
		}).then(response => {
			if (!response.ok) {
				return response.json().then(json => Promise.reject(json));
			}
			return response;
		}).then(json => json.json()).then(newList => {
			this.setState(prevState => {
				const listIndex = prevState.lists.findIndex(list => list._id === newList._id);
				prevState.lists[listIndex] = newList;
				prevState.toggleOpenShoppingList = newList;
				return prevState;
			});
		}).catch(err => {
			window.M.toast({classes: 'red', html: err.error.message});
		});
	}

	deleteItemFromList(itemId) {
		fetch(`${apiBase}/shopping-lists/${this.state.toggleOpenShoppingList._id}/item/${itemId}`, {
			method: 'DELETE',
			headers: { 'Authorization': localStorage.token }
		}).then(response => {
			if (response.status !== 204) {
				return response.json().then(json => Promise.reject(json));
			}
			this.setState(prevState => {
				const itemIndex = prevState.toggleOpenShoppingList.items.findIndex(item => item._id === itemId);
				prevState.toggleOpenShoppingList.items.splice(itemIndex, 1);
				return {toggleOpenShoppingList: prevState.toggleOpenShoppingList};
			});
		}).catch(err => {
			window.M.toast({classes: 'red', html: err.error.message});
		});
	}

	addShoppingList(formData) {
		fetch(`${apiBase}/shopping-lists`, {
			method: 'POST',
			headers: {
				'Authorization': localStorage.token,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(formData)
		}).then(response => {
			if (!response.ok) {
				return response.json().then(json => Promise.reject(json));
			}
			return response;
		}).then(json => json.json()).then(newList => {
			if (!newList) {
				return Promise.reject({ error: { message: 'Oops! Something went wrong. Couldn\'t create list' } });
			}
			this.setState({
				lists: [...this.state.lists, newList],
				showModal: false
			});
		}).catch(err => {
			window.M.toast({classes: 'red', html: err.error.message});
		});
	}

	deleteShoppingList(id) {

		fetch(`${apiBase}/shopping-lists/${id}`, {
			method: 'DELETE',
			headers: { 'Authorization': localStorage.token }
		}).then(response => {
			if (!response.ok) {
				return response.json().then(json => Promise.reject(json));
			}
			this.setState(prevState => {
				const listIndex = this.state.lists.findIndex(list => list._id === id);
				prevState.lists.splice(listIndex, 1);
				return { items: prevState.lists };
			});
		}).catch(err => {
			window.M.toast({classes: 'red', html: err.error.message});
		});
	}

	toggleOpenShoppingList(id = null) {
		if (this.state.toggleOpenShoppingList === null && id !== null) {
			fetch(`${apiBase}/shopping-lists/${id}`, {
				headers: { 'Authorization': localStorage.token }
			}).then(response => {
				if (!response.ok) {
					return response.json().then(json => Promise.reject(json));
				}
				return response;
			}).then(json => json.json()).then(list => {
				this.setState({toggleOpenShoppingList: list});
			}).catch(err => {
				window.M.toast({classes: 'red', html: err.error.message});
			});
		} else {
			return this.setState({toggleOpenShoppingList: null});
		}
	}

	toggleOpenShoppingListModal(component) {
		this.setState({ showModal: !this.state.showModal, modalComponent: component });
	}

	publishShoppingListToggle(id, publish) {
		fetch(`${apiBase}/shopping-lists/${id}`, {
			method: 'PUT',
			headers: {
				'Authorization': localStorage.token,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ public: publish })
		}).then(response => {
			if (!response.ok) {
				return response.json().then(json => Promise.reject(json));
			}
			return response;
		}).then(json => json.json()).then(updatedList => {
			this.setState(prevState => {
				const listIndex = prevState.lists.findIndex(list => list._id === updatedList._id);
				prevState.lists[listIndex] = updatedList;
				return prevState;
			});
			this.state.ws.send('Hello Server!');
		}).catch(err => {
			window.M.toast({classes: 'red', html: err.error.message});
		});
	}

	componentDidMount() {
		this.setState({ ws: new WebSocket('ws://localhost:3001/') });
		this.getShoppingLists();
	}

    render() {
        return (
        	<div className="container" style={{marginBottom: 60}}>
    		{this.state.toggleOpenShoppingList === null ? (
    			<div>
	        		<div className="row">
	        			<div className="col s12">
	        				<h3>Dine handlelister</h3>
		        			<button className="waves-effect waves-dark btn white black-text" onClick={() => this.toggleOpenShoppingListModal('shoppingListNewForm')}>Ny liste</button>
	        			</div>
	        		</div>
	        		<ShoppingLists lists={this.state.lists} publishToggleFn={this.publishShoppingListToggle} openFn={this.toggleOpenShoppingList} deleteFn={this.deleteShoppingList} />
	        		<ShoppingListsPublic openFn={this.toggleOpenShoppingList} />
    			</div>
    		) : (
    			<ShoppingListContent list={this.state.toggleOpenShoppingList} addFn={this.addItemToList} deleteFn={this.deleteItemFromList} closeFn={this.toggleOpenShoppingList} />
    		)}
	        	{this.state.showModal && <Backdrop closeModal={this.toggleOpenShoppingListModal} />}
	        	{this.state.showModal && <Modal object={this.state.openList} handleSubmit={this.addShoppingList} component={this.state.modalComponent} closeModal={this.toggleOpenShoppingListModal} />}
        	</div>
        );
    }
}

export default LoggedIn;
