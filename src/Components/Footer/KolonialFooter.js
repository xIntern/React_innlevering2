import React from 'react';

const KolonialFooter = props => {
	return (
		<footer>
			<div className="container">
				<div className="valign-wrapper center">
					Takk til
					<a href="https://kolonial.no/"><img width="280px" src={require('./kolonial.png')} alt="kolonial.no" /></a>
				</div>
			</div>
		</footer>
	);
}

export default KolonialFooter;
