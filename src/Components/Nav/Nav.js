import React from 'react';

const Nav = props => {
    return (
		<nav>
		    <div className="nav-wrapper">
		        <a href="/" className="brand-logo">Handleliste</a>
		            {props.loggedIn ? (
		        		<ul id="nav-mobile" className="right hide-on-small-only">
			            	<li><p>Hei, {props.user.name}</p></li>
			            	<li className="waves-effect waves-dark"><a onClick={() => { props.logoutFn() }}>Logg ut</a></li>
		        		</ul>
	            	) : (
		        		<ul id="nav-mobile" className="right hide-on-med-and-down">
		            		<li className="waves-effect waves-dark"><a onClick={ () => props.toggleModal('registerForm') }>Ny bruker</a></li>
		            		<li className="waves-effect waves-dark"><a onClick={ () => props.toggleModal('loginForm') }>Logg inn</a></li>
		        		</ul>
		        	)}
		    </div>
		</nav>
    );
}

export default Nav;