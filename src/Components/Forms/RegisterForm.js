import React from 'react';
import AuthAlert from '../Alerts/AuthAlert';

const RegisterForm = props => {
	return (
		<form onSubmit={props.handleSubmit}>
			<div className="row">
				<div className="col s12">
					<h4>Ny bruker</h4>
				</div>
			</div>
			{props.error !== null && <AuthAlert title={props.error.title} message={props.error.message} />}
			<div className="row">
				<div className="input-field col s12">
					<input id="name" type="text" placeholder="Navn" autoComplete="off" onChange={props.handleChange} required />
					<label htmlFor="name">Navn</label>
				</div>
			</div>
			<div className="row">
				<div className="input-field col s12">
					<input id="email" type="email" placeholder="E-post" autoComplete="off" onChange={props.handleChange} required />
					<label htmlFor="email">E-post</label>
				</div>
			</div>
			<div className="row">
				<div className="input-field col s12">
					<input id="password" type="password" placeholder="Passord" autoComplete="off" onChange={props.handleChange} pattern=".{3,}" required />
					<label htmlFor="password">Passord</label>
				</div>
			</div>
			<div className="row">
				<div className="input-field col s12">
					<input id="repeat-password" type="password" placeholder="Gjenta passord" autoComplete="off" onChange={props.handleChange} pattern=".{3,}" required />
					<label htmlFor="repeat-password">Gjenta passord</label>
				</div>
			</div>
			<div className="row">
				<div className="input-field col s12">
					<button className="waves-effect waves-light btn indigo right" type="submit">Opprett bruker</button>
					{props.modal && <button className="btn-flat right" type="reset" onClick={() => props.closeModal()}>Avbryt</button>}
				</div>
			</div>
		</form>
	);
}

export default RegisterForm;
