import React, { Component } from 'react';
import Item from '../ShoppingList/Item';

const delay = (() => {
  let timer = 0;
  return (callback, ms) => {
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

const apiBase = 'http://localhost:3001';

class ItemSearch extends Component {

	constructor(props) {
		super(props);
		this.state = {
			searchFocus: false,
			searchStr: '',
			items: []
		};
	}

	searchItem() {
		if (this.state.searchStr.length < 2) {
			this.setState({ items: [] });
			return;
		}
		fetch(`${apiBase}/search/item?name=${this.state.searchStr}`, {
			headers: {
				'Authorization': localStorage.token
			}
		}).then(response => {
			if (!response.ok) {
				console.error(response);
				return;
			}
			return response;
		}).then(json => json.json()).then(items => {
			this.setState({items: items});
		});
	}

	render() {
		return (
			<div className="relative">
				<div className="row">
					<div className="input-field col s12">
						<label className="active" htmlFor="item-search">Legg til varer i listen</label>
						<input
							id="item-search"
							type="search"
							placeholder="Søk i varer"
							autoComplete="off"
							onChange={e => {
								this.setState({searchStr: e.target.value}, () => {
									delay(() => {
										this.searchItem();
									}, 300);
								});
							}}
							onFocus={() => this.setState({ searchFocus: true })}
							onBlur={() => this.setState({ searchFocus: false })}
						/>
					</div>
				</div>
				{(this.state.items.length > 0 && this.state.searchStr.length >= 2) &&
					<ul id="item-search-result" className="collection absolute">
						{this.state.items.map((item, i) => {
							return <Item
								key={i}
								id={item._id}
								name={item.name}
								image={(item.images) ? item.images[0].thumbnail.url : item.image}
								isSearch={true}
								actionFn={this.props.addItemToList}
								actionText="Legg til"
							/>
						})}
					</ul>
				}
			</div>
		);
	}
}

export default ItemSearch;
