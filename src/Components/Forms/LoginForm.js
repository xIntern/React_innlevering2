import React from 'react';
import AuthAlert from '../Alerts/AuthAlert';

const LoginForm = props => {
	return (
		<form onSubmit={props.handleSubmit}>
			<div className="row">
				<div className="col s12">
					<h4>Logg inn</h4>
				</div>
			</div>
			{props.error !== null && <AuthAlert title={props.error.title} message={props.error.message} />}
			<div className="row">
				<div className="input-field col s12">
					<label htmlFor="email">Email</label>
					<input id="email" type="email" autoComplete="off" placeholder="Email" onChange={props.handleChange} required />
				</div>
			</div>
			<div className="row">
				<div className="input-field col s12">
					<label htmlFor="password">Password</label>
					<input id="password" type="password" autoComplete="off" placeholder="Password" onChange={props.handleChange} required />
				</div>
			</div>
			<div className="row">
				<div className="input-field col s12">
					<button className="waves-effect waves-light btn indigo right" type="submit">Logg inn</button>
					{props.modal && <button className="btn-flat right" type="reset" onClick={() => props.closeModal()}>Avbryt</button>}
				</div>
			</div>
		</form>
	);
}

export default LoginForm;
