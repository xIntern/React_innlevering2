import React from 'react';
import ItemSearch from './ItemSearch';

const ShoppingListItem = props => {
    return (
    	<form id="edit-shopping-list">
    		<div className="title">
    			<h5>Endre handleliste</h5>
    		</div>
    		<div id="shopping-list-content">
                <div className="row">
                    <div className="input-field col s12">
                        <label className="active" htmlFor="name">Navn</label>
                        <input id="name" type="text" defaultValue={props.list.name} required />
                    </div>
                </div>
                <ItemSearch shoppingListId={props.list._id} />
                {props.list.items.length ? (
                    <div className="">
                        <h5>Varer i listen</h5>
                        <table>
                            <tbody>
                                {props.list.items.map((item, i) => (
                                    <tr key="{i}">
                                        <td>
                                            <input
                                                type="number"
                                                value={item.amount}
                                                min="1"
                                                onChange={e => {
                                                    props.updateFn(item._id, e.target.value);
                                                }}
                                            />
                                        </td>
                                        <td>{item.item.name}</td>
                                        <td>
                                            <img src={item.item.image} height="66px" alt={item.item.name} />
                                        </td>
                                        <td>
                                            <button
                                                className="btn-flat red-text"
                                                onClick={() => {
                                                    props.deleteFn(item._id);
                                                }}
                                            >
                                                Slett
                                            </button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                 ) : (
                    <h5 className="center-align">Ingen varer i listen</h5>
                )}
    		</div>
            <div className="row">
                <div className="input-field col s12">
        			<button className="btn-flat right" type="submit">Lagre</button>
        			<button className="btn-flat right" type="reset" onClick={() => props.closeModal()}>Avbryt</button>
                </div>
            </div>
    	</form>
    );
}

export default ShoppingListItem;
