import React from 'react';

const LoginForm = props => {
	return (
		<form onSubmit={props.handleSubmit}>
			<div className="row">
				<div className="col s12">
					<h4>Ny handleliste</h4>
				</div>
			</div>
			<div className="row">
				<div className="input-field col s12">
					<label className="active" htmlFor="name">Navn</label>
					<input id="name" type="text" placeholder="Navn" onChange={props.handleChange} required />
				</div>
			</div>
			<div className="row">
				<div className="input-field col s12">
					<button className="btn-flat right" type="submit">Lagre</button>
					<button className="btn-flat right" type="reset" onClick={() => props.closeModal()}>Avbryt</button>
				</div>
			</div>
		</form>
	);
}

export default LoginForm;
