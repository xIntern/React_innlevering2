import React from 'react';

const Backdrop = props => {
    return (
    	<div className="modal-overlay" onClick={() => props.closeModal()}></div>
    );
}

export default Backdrop;
