import React from 'react';

const Modal2 = props => {
	return (
		<div className="modal">
			<div className="modal-content">
				<h4>{props.title}</h4>
				<p>{props.message}</p>
			</div>
			<div className="modal-footer">
				<a href="#!" className="modal-action modal-close waves-effect waves-green btn-flat" onClick={() => props.closeFn()}>Close</a>
			</div>
		</div>
	);
}

export default Modal2;
