import React, { Component } from 'react';
import Login from '../Forms/LoginForm';
import Register from '../Forms/RegisterForm';
import ShoppingListNew from '../Forms/ShoppingListNewForm';
import ShoppingListEdit from '../Forms/ShoppingListEditForm';

class Modal extends Component {

	constructor(props) {
		super(props);
		this.state = {};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(e) {
		e.preventDefault();
		this.props.handleSubmit(this.state);
	}

	handleChange(e) {
		this.setState({ [e.target.id]: e.target.value });
	}

	render() {
		return (
			<div id="modal" className="modal col s12 m3">
				<div className="modal-content">
					{this.props.component === 'registerForm' && <Register modal={true} handleChange={this.handleChange} handleSubmit={this.handleSubmit} closeModal={this.props.closeModal} />}
					{this.props.component === 'loginForm' && <Login modal={true} handleChange={this.handleChange} handleSubmit={this.handleSubmit} closeModal={this.props.closeModal} />}
					{this.props.component === 'shoppingListNewForm' && <ShoppingListNew handleChange={this.handleChange} handleSubmit={this.handleSubmit} closeModal={this.props.closeModal} />}
					{this.props.component === 'shoppingListEditForm' && <ShoppingListEdit handleSearch={this.props.handleSearch} list={this.props.object} handleChange={this.handleChange} handleSubmit={this.handleSubmit} closeModal={this.props.closeModal} />}
				</div>
			</div>
		);
	}
}

export default Modal;