import React, { Component } from 'react';
import LoginForm from './Forms/LoginForm';
import RegisterForm from './Forms/RegisterForm';

// const apiBase = 'http://localhost:3001';

class NotLoggedIn extends Component {

	constructor(props) {
		super(props);
		this.state = {
			register: false,
			formData: {}
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(e) {
		e.preventDefault();
		if (this.state.register) {
			this.props.registerFn(this.state.formData);
		} else {
			this.props.loginFn(this.state.formData);
		}
	}

	handleChange(e) {
		e.persist();
		this.setState(prevState => prevState.formData[e.target.id] = e.target.value);
	}

    render() {
        return (
        	<div style={{maxWidth: 360, margin: '0 auto'}}>
	        	<div className="z-depth-1 grey lighten-4">
		    		{this.state.register ? (
		    			<RegisterForm handleSubmit={this.handleSubmit} handleChange={this.handleChange} error={this.props.error} />
		    		) : (
		    			<LoginForm handleSubmit={this.handleSubmit} handleChange={this.handleChange} error={this.props.error} />
		    		)}
	        	</div>
	    		<a
	    			href="#!"
	    			style={{width: '100%', display: 'inline-block', padding: '20px 0',}}
	    			className="center-align"
	    			onClick={() => {
	    				this.setState({register: !this.state.register});
	    				this.props.closeError();
	    			}}
	    		>
	    		{this.state.register ? 'Logg inn' : 'Lag ny bruker'}
	    		</a>
        	</div>
        );
    }
}

export default NotLoggedIn;
