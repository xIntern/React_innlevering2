import React from 'react';

const ShoppingListPublic = props => {
    return (
        <tr>
            <td>
                <button
                    className="waves-effect waves-dark btn-flat"
                    onClick={() => {
                        props.openFn(props.id);
                    }}
                >
                    {props.name}
                </button>
            </td>
            <td>
                {props.items.length} vare(r)
            </td>
            <td>
                <p>{props.user}</p>
            </td>
        </tr>
    );
}

export default ShoppingListPublic;
