import React from 'react';

const Item = props => {
	return (
		<li className="collection-item">
			<div className="item-image">
				<img src={props.image} alt={props.name} className="" />
			</div>
			<p className="title">{props.name}</p>
			<div className="secondary-content">
				{typeof(props.isSearch) === 'undefined' &&
		            <label>
		                <input
		                    type="checkbox"
		                />
		                <span>Handlet</span>
		            </label>
				}
				{props.actionText !== '' &&
					<button
						className="waves-effect waves-dark btn-flat"
						onClick={() => {
							props.actionFn(props.id);
						}}
					>
						{props.actionText}
					</button>
				}
			</div>
		</li>
	);
}

export default Item;
