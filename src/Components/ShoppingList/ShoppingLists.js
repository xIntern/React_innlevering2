import React from 'react';
import ShoppingList from './ShoppingList';
const ShoppingLists = props => {
    return (
        <div className="row">
            <div className="col s12">
                {props.lists.length ? (
                    <table className="bordered">
                        <tbody>
                            {props.lists.map((list, i) => <ShoppingList
                                key={list._id}
                                id={list._id}
                                name={list.name}
                                items={list.items}
                                public={list.public}
                                publishToggleFn={props.publishToggleFn}
                                openFn={props.openFn}
                                deleteFn={props.deleteFn}
                            />)}
                        </tbody>
                    </table>
                ) : (
                    <h4 className="center-align">Du har ingen handlelister</h4>
                )}
            </div>
        </div>
    );
}

export default ShoppingLists;
