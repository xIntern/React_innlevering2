import React from 'react';
import Item from './Item';
import ItemSearch from '../Forms/ItemSearch';

const ShoppingListContent = props => {
	return (
		<div className="shopping-list">
			<div className="shopping-list-header">
				<button className="waves-effect waves-dark btn-flat btn-back" onClick={() => props.closeFn()}><i className="material-icons">keyboard_arrow_left</i><p>Tilbake</p></button>
				<h4>{props.list.name}</h4>
			</div>
			{JSON.parse(localStorage.user).id === props.list.addedBy.user &&
				<div className="item-search">
					<ItemSearch shoppingListId={props.list._id} addItemToList={props.addFn} />
				</div>
			}
			<div className="shopping-list-content">
			{props.list.items.length ? (
				<ul>
					{props.list.items.map((item, i) => <Item key={item._id} id={item._id} name={item.item.name} image={item.item.image} actionFn={props.deleteFn} actionText={JSON.parse(localStorage.user).id === props.list.addedBy.user ? 'Fjern' : ''} />)}
				</ul>
			) : (
				<h5 className="center-align">Handlelisten er tom</h5>
			)}
			</div>
		</div>
	);
}

export default ShoppingListContent;
