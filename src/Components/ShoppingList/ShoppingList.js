import React from 'react';

const ShoppingList = props => {
    return (
        <tr>
            <td>
                <button
                    className="waves-effect waves-dark btn-flat"
                    onClick={() => {
                        props.openFn(props.id);
                    }}
                >
                    {props.name}
                </button>
            </td>
            <td>
                {props.items.length} vare(r)
            </td>
            <td>
                <p>
                    <label>
                        <input
                            type="checkbox"
                            checked={props.public}
                            onChange={e => props.publishToggleFn(props.id, e.target.checked)}
                        />
                        <span>Offentlig</span>
                    </label>
                </p>
            </td>
            <td>
                <button className="waves-effect waves-dark btn-flat red-text" onClick={() => props.deleteFn(props.id)}>Slett</button>
            </td>
        </tr>
    );
}

export default ShoppingList;
