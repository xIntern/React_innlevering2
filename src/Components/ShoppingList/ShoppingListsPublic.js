import React, { Component } from 'react';
import ShoppingListPublic from './ShoppingListPublic';

class ShoppingListsPublic extends Component {

	constructor(props) {
		super(props);
		this.state = {
			lists: []
		};
	}

	componentDidMount() {
		const ws = new WebSocket('ws://localhost:3001/');

		ws.onmessage = message => {
			const lists = JSON.parse(message.data);
			this.setState({
				lists: lists
			});
		}
	}

    render() {
        return (
	        <div className="row">
	            <div className="col s12 grey lighten-4">
	            	<h5>Offentlige handlelister</h5>
	                {this.state.lists.length ? (
	                    <table className="bordered">
	                        <tbody>
	                            {this.state.lists.map((list, i) => <ShoppingListPublic
	                                key={list._id}
	                                id={list._id}
	                                name={list.name}
	                                items={list.items}
	                                user={list.addedBy.user.name}
	                                openFn={this.props.openFn}
	                            />)}
	                        </tbody>
	                    </table>
	                ) : (
	                    <p>Det finnes ingen offentlige handlelister. Kanskje du vil lage den første offentlig listen?</p>
	                )}
	            </div>
	        </div>
        );
    }
}

export default ShoppingListsPublic;
