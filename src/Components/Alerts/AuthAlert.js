import React from 'react';

const AuthAlert = props => {
	return (
		<div className="row">
			<div className="col s12 orange lighten-2">
				<p className="center-align" style={{marginTop: 15, marginBottom: 15}}><b>{props.title}: </b>{props.message}</p>
			</div>
		</div>
	);
}

export default AuthAlert;
