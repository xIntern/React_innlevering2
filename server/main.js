const express = require('express');
const bcrypt = require('bcryptjs');
const jwtSimple = require('jwt-simple');
const jsonParser = require('body-parser').json();
const mongooseDB = require('./db/db');
const request = require('request');
const ws = require('ws');


const app = express();
app.use(jsonParser);
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
});

const db = new mongooseDB();

// const prodDB = new mongooseDB('a10');
// const testDB = new mongooseDB('a10-test');
// let db;

// const MWTesting = (req, res, next) => {
//     db = (req.originalUrl.indexOf('/test') > -1) ? testDB : prodDB;
//     next();
// };
// app.use(MWTesting);

const MWTokenExists = (req, res, next) => {
	const allowedMethods = ['OPTIONS', 'HEAD'];
	const allowedRoutes = ['/login', '/users'];
	const token = req.headers.authorization;

	if (allowedMethods.indexOf(req.method) > -1) {
		next();
		return;
	}
	if (!token && (req.method === 'POST' && allowedRoutes.indexOf(req.originalUrl) > -1)) {
		next();
		return;
	}
	if (token) {
		next();
		return;
	}

	return res.status(401).send({message: 'No token'});
};
app.use(MWTokenExists);

const jwtSecret = 'dont touch this unless you know what you are doing!';


const getUserFromToken = token => {
	return new Promise((resolve, reject) => {
		if (!token) {
			return reject('No token');
		}

		const payload = jwtSimple.decode(token, jwtSecret);
		const id = payload.userId;

		db.getUserById(id).then(user => {
			if (user === null) {
				return reject('Couldnt find user');
			}

			return resolve(user);
		})
	});
}

const makeJWT = id => {
	if (!id) {
		return false;
	}
	return jwtSimple.encode({userId: id}, jwtSecret);
}

// app.use(require('./routes'));

app.route('/users')
.get((req, res) => {
	db.getUsers().then(users => res.send(users));
})
.post((req, res) => {
	const user = req.body;

	if (!user.name) {
		res.status(400).send({message: 'Username is required'});
		return;
	}
	if (!user.password) {
		res.status(400).send({message: 'Password is required'});
		return;
	}
	if (!user.email) {
		res.status(400).send({message: 'Email is required'});
		return;
	}

	user.password = bcrypt.hashSync(user.password);

	db.addUser(user).then(newUser => res.status(201).send({
		token: makeJWT(newUser._id),
		user: {
			id: newUser._id,
			name: newUser.name,
			email: newUser.email
		}
	}))
	.catch(err => {
		if (err.code === 11000) {
			return res.status(409).send({
				error: {
					title: 'Register error',
					message: 'Email is already registered'
				}
			});
		}
	});
});

app.post('/login', (req, res) => {
	const user = req.body;
	if (!user.email) {
		return res.status(400).send({
			error: {
				title: 'Login error',
				message: 'Email is required'
			}
		});
	}
	if (!user.password) {
		return res.status(400).send({
			error: {
				title: 'Login error',
				message: 'Password is required'
			}
		});
	}

	db.getUserByEmail(user.email).then(dbUser => {
		if (!dbUser) {
			return res.status(404).send({
				error: {
					title: 'Login error',
					message: 'Wrong email or password. Please check your inputs'
				}
		});
		}

		if (!dbUser.password) {
			return res.status(500).send({
				error: {
					title: 'Server error',
					message: 'Something went wrong with the server'
				}
		});
		}

		const passwordMatches = bcrypt.compareSync(user.password, dbUser.password);
		if (!passwordMatches) {
			return res.status(401).send({
				error: {
					title: 'Login error',
					message: 'Wrong email or password. Please check your inputs'
				}
		});
		}

		res.send({
			token: makeJWT(dbUser._id),
			user: {
				id: dbUser._id,
				name: dbUser.name,
				email: dbUser.email
			}
		});
	}).catch(err => {
		console.log(err);
		res.status(400).send(err);
	});
});

app.get('/shopping-lists', (req, res) => {
	getUserFromToken(req.headers.authorization)
		.then(user => db.getShoppingListsFromUserId(user._id))
		.then(lists => res.send(lists))
		.catch(err => res.sendStatus(500));
});

app.route('/shopping-lists/:id?')
.get((req, res) => {
	const listId = req.params.id;
	if (!listId) {
		return res.status(400).send({message: 'No shopping list id'});
	}
	console.log(listId);
	db.getShoppingListById(listId).then(lists => {
		res.send(lists);
	});
})
.post((req, res) => {
	getUserFromToken(req.headers.authorization).then(user => {
		if (typeof(req.params.id) === 'undefined') {
			// Create a new shopping list
			const shoppingList = req.body;
			shoppingList.items = [];
			shoppingList.members = (typeof(shoppingList.members) === 'undefined') ? [] : shoppingList.members.split(' ');
			shoppingList.addedBy = {
				user: user._id
			};
			return Promise.all([user, db.addShoppingList(shoppingList)]);
		} else {
			// Add item to shopping list
			return Promise.all([user, db.addItemToShoppingList(req.params.id, req.body)]);
		}
	}).then(response => {
		const user = response[0];
		const newShoppingList = response[1];
		if (typeof(newShoppingList) === 'undefined') {
			return Promise.reject({
				status: 500,
				title: 'Saving shopping list error',
				message: 'Couldn\'t save shopping list'
			});
		}
		if (user._id.toString() != newShoppingList.addedBy.user.toString()) {
			return Promise.reject({
				status: 403,
				title: 'Edit shopping list error',
				message: 'You are not permitted to edit this shopping list',
				x: user._id + ' != ' + newShoppingList.addedBy.user + ' = ' + (user._id != newShoppingList.addedBy.user)
			});
		}
		return db.getShoppingListById(newShoppingList._id);
	}).then(shoppingList => {
		res.send(shoppingList);
	}).catch(err => {
		console.log('Shopping list error', err);
		res.status(err.status).send({error: err});
	});
})
.put((req, res) => {
	const listId = req.params.id;
	const newList = req.body;
	if (!listId) {
		return res.status(400).send('No shopping list id');
	}

	if (!newList) {
		return res.status(400).send('No shopping list object');
	}

	db.editShoppingList(listId, newList).then(updatedList => {
		if (updatedList === null) {
			return res.status(500).send(updatedList);
		}
		res.send(updatedList);
	});
})
.delete((req, res) => {
	const listId = req.params.id;
	if (!listId) {
		return res.status(400).send('No shopping list id');
	}
	console.log(listId);

	db.deleteShoppingList(listId).then(deletedItem => {
		if (!deletedItem) {
			return res.status(500).send('Error deleting list');
		}
		return res.sendStatus(204);
	});
});

app.get('/shopping-lists/recent', (req, res) => {
	db.getPublicShoppingLists().then(lists => {
		if (!lists.length) {
			return res.status(404).send({
				error: {
					title: 'No public lists',
					message: 'Theres no public lists available. Try publishing one of your own shopping list.'
				}
			});
		}

		res.send(lists);
	});
});

app.delete('/shopping-lists/:listId/item/:itemId', (req, res) => {
	getUserFromToken(req.headers.authorization).then(user => {
		return Promise.all([user, db.getShoppingListById(req.params.listId)]);
	}).then(response => {
		const user = response[0];
		const list = response[1];
		if (user._id === list.addedBy.user) {
			return res.sendStatus(401);
		}
		console.log(list.items);
		const itemIndex = list.items.findIndex(item => item._id == req.params.itemId);
		if (itemIndex < 0) {
			return Promise.reject({message: 'Couldn\'t find item in list'});
		}
		list.items.splice(itemIndex, 1);
		return list.save();
	}).then(savedList => {
		res.sendStatus(204);
	}).catch(err => {
		console.error(err);
		res.status(404).send(err);
	});
});

app.get('/items', (req, res) => {
	db.getAllItems().then(items => res.send(items));
});

app.route('/items/:id?')
.get((req, res) => {
	const itemId = req.params.id;
	if (!itemId) {
		res.status(400).send('Item ID is required');
	}

	db.getItemById(itemId).then(item => {
		if (item === null) {
			res.status(404).send('Item not found');
			return;
		}
		res.send(item);
	});
})
.post((req, res) => {
	const item = req.body;
	db.addItem(item).then(item => res.status(201).send(item));
})
.delete((req, res) => {
	const itemId = req.params.id;
	if (!itemId) {
		res.status(400).send('Item ID is required');
		return;
	}

	db.deleteItem(itemId).then(deletedItem => {
		if (!deletedItem) {
			res.status(500).send('Something went wrong while deleting item');
			return;
		}
		res.status(204).send();
	})
});

app.get('/search/item', (req, res) => {
	const itemName = req.query.name;
	db.searchItems(itemName).then(results => {
		res.send(results);
	});
});

app.get('*', (req, res) => {
    res.status(404).send('404 Not found');
});

const port = 3001;
const server = app.listen(port, () => console.log(`Listening on port ${port}`));

const wsServer = new ws.Server({ server: server });

let sockets = [];

wsServer.on('connection', socket => {
	sockets.push(socket);
	db.getPublicShoppingLists().then(lists => {
		socket.send(JSON.stringify(lists));
	});

	socket.on('message', data => {
		db.getPublicShoppingLists().then(lists => {
			sockets.forEach(socket => socket.send(JSON.stringify(lists)));
		});
	});

	socket.on('close', () => {
		sockets = sockets.filter(savedSocket => savedSocket !== socket);
	});
});
