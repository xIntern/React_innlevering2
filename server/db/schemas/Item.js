const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const itemSchema = Schema({
    name: {
        type: String,
        unique: true
    },
    image: String
});
itemSchema.index({"name": "text"});

module.exports = itemSchema;
