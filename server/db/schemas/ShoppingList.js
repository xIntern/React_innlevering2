const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const shoppingListSchema = Schema({
    name: String,
    public: {
        type: Boolean,
        default: false
    },
    items: [{
        item: {
            type: Schema.Types.ObjectId,
            ref: 'Item'
        },
        amount: Number
    }],
    members: {
        type: [String],
        default: []
    },
    addedBy: {
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        time: {
            type: Date,
            default: Date.now
        }
    }
}, {
    timestamps: {
        createdAt: 'addedBy.time'
    }
});

module.exports = shoppingListSchema;
