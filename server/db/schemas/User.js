const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = Schema({
    name: String,
    password: String,
    email: {
        type: String,
        unique: true
    },
    role: {
        type: String,
        default: 'user'
    }
});

module.exports = userSchema;
