const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const userSchema = require('./schemas/User');
const itemSchema = require('./schemas/Item');
const shoppingListSchema = require('./schemas/ShoppingList');

class DB {
    constructor(dbName) {
        // const conn = mongoose.createConnection(`mongodb://localhost/${dbName}`);
        const conn = mongoose.createConnection(`mongodb://waaand14:pg6300Eksamen@ds133496.mlab.com:33496/handleliste`);
        this.User = conn.model('User', userSchema);
        this.Item = conn.model('Item', itemSchema);
        this.ShoppingList = conn.model('ShoppingList', shoppingListSchema);
    }

    getUsers() {
        return this.User.find().exec();
    }

    getUserByEmail(email) {
        return this.User.findOne({'email': email}).exec();
    }

    getUserByName(username) {
        return this.User.findOne({'name': username}).exec();
    }

    getUserById(id) {
        return this.User.findById(id).exec();
    }

    addUser(user) {
        return this.User(user).save();
    }

    deleteUser(id) {
        return this.User.findByIdAndRemove(id).exec();
    }

    editUser(id, newUser) {
        this.getUserById(id).then(user => {
            Object.assign(user, newUser);
            return user.save();
        });
    }


    getShoppingLists() {
        return this.ShoppingList.find().populate('addedBy.user').exec();
    }

    getPublicShoppingLists() {
        return this.ShoppingList.find({'public': true}).populate('addedBy.user').sort('-updatedAt').limit(10).exec();
    }

    getShoppingListsFromUserId(userId) {
        return this.ShoppingList.find({'addedBy.user': userId}).exec();
    }

    getShoppingListByName(name) {
        return this.ShoppingList.findOne({'name': name}).exec();
    }

    getShoppingListById(id) {
        return this.ShoppingList.findById(id).populate('items.item').exec();
    }

    addShoppingList(list) {
        return this.ShoppingList(list).save();
    }

    deleteShoppingList(id) {
        return this.ShoppingList.findByIdAndRemove(id).exec();
    }

    editShoppingList(id, editedList) {
        return this.getShoppingListById(id).then(list => {
            Object.assign(list, editedList);
            return list.save();
        });
    }

    addItemToShoppingList(listId, item) {
        return this.getShoppingListById(listId).then(list => {
            list.items.push(item);
            return list.save();
        });
    }

    deleteItemFromShoppingList(listId, itemId) {
        return this.getShoppingListById(listId).then(list => {
            const itemIndex = list.items.findIndex(item => item._id == itemId);
            list.items.splice(itemIndex, 1);
            return list.save();
        });
    }


    getAllItems() {
        return this.Item.find().exec();
    }

    getItemsByName(name) {
        return this.Item.find({'name': name}).exec();
    }

    getItemById(itemId) {
        return this.Item.findById(itemId).exec();
    }

    addItem(item) {
        return this.Item(item).save();
    }

    deleteItem(id) {
        return this.Item.findByIdAndRemove(id).exec();
    }

    editItem(id, newItem) {
        this.getItemById(id).then(item => {
            Object.assign(item, newItem);
            return item.save();
        });
    }

    searchItems(name) {
        return this.Item.find({
            'name': new RegExp(name, 'i')
        }).exec();
    }
}

module.exports = DB;
