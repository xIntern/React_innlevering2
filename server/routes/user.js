const db = require('../db/db');
const router = require('express').Router();

router.get('/', (req, res) => {
	db.getUsers().then(users => res.send(users));
});

router.post('/', (req, res) => {
	const user = req.body;

	if (!user.name) {
		res.status(400).send({message: 'Username is required'});
		return;
	}
	if (!user.password) {
		res.status(400).send({message: 'Password is required'});
		return;
	}
	if (!user.email) {
		res.status(400).send({message: 'Email is required'});
		return;
	}

	user.password = bcrypt.hashSync(user.password);

	db.addUser(user).then(newUser => res.status(201).send({
		token: makeJWT(newUser._id),
		user: {
			name: newUser.name,
			email: newUser.email
		}
	}))
	.catch(err => {
		if (err.code === 11000) {
			res.status(409).send({message: 'Email already exists'});
			return;
		}
	});
});

router.get('/:id', (req, res) => {

});

module.exports = router;
