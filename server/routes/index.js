const router = require('express').Router();

router.use('/users', require('./user'));
// router.use('/login', require('./routes/auth'));
// router.use('/shopping-list', require('./routes/shopping_list'));
// router.use('/item', require('./routes/item'));

module.exports = router;
